import { useState, useEffect } from "react";
import styles from "./App.module.scss";
import Header from "./components/Header/Header";
import AppRoutes from "./AppRoutes";

const App = () => {
  const [cards, setCards] = useState([]);
  const [isCardsLoading, setIsCardsLoading] = useState(true);
  const [favorites, setFavorites] = useState([]);
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [modalProps, setModalProps] = useState({});
  const [cart, setCart] = useState([]);

  useEffect(() => {
    const getData = async () => {
      try {
        const data = await fetch("./data.json").then((res) => res.json());
        const favorites = JSON.parse(localStorage.getItem("favorites"));
        const cart = JSON.parse(localStorage.getItem("cart"));

        if (favorites) {
          setFavorites(favorites);
          data.map((card) => {
            if (favorites.find((el) => el.code === card.code)) {
              card.isFavorite = true;
            }
            return card;
          });
        }

        if (cart) {
          setCart(cart);
          data.map((card) => {
            const item = cart.find((el) => el.code === card.code);
            if (item) {
              card.inCart = true;
              card.qty = item.qty;
            }
            return card;
          });
        }

        setCards(data);
        localStorage.setItem("cards", JSON.stringify(data));
        setIsCardsLoading(false);
      } catch (err) {
        console.error(err);
      }
    };
    getData();
  }, []);

  const addToFavorites = (card) => {
    setFavorites((current) => {
      const favorites = [...current];
      const index = favorites.findIndex((el) => el.code === card.code);

      if (index === -1) {
        setCards((current) => {
          const cards = [...current];
          const i = cards.findIndex((el) => el.code === card.code);
          cards[i].isFavorite = true;
          localStorage.setItem("cards", JSON.stringify(cards));
          return cards;
        });
        favorites.push({ ...card, isFavorite: true });
      } else {
        setCards((current) => {
          const cards = [...current];
          const i = cards.findIndex((el) => el.code === card.code);
          cards[i].isFavorite = false;
          localStorage.setItem("cards", JSON.stringify(cards));
          return cards;
        });
        favorites.splice(index, 1);
      }
      localStorage.setItem("favorites", JSON.stringify(favorites));
      return favorites;
    });
  };

  const toggleModal = (value) => {
    setIsOpenModal(value);
  };

  const addToCart = (card) => {
    setCart((current) => {
      const cart = [...current];
      const index = cart.findIndex((el) => el.code === card.code);

      if (index === -1) {
        setCards((current) => {
          const cards = [...current];
          const i = cards.findIndex((el) => el.code === card.code);
          cards[i].inCart = true;
          cards[i].qty = 1;
          localStorage.setItem("cards", JSON.stringify(cards));
          return cards;
        });
        cart.push({ ...card, inCart: true, qty: 1 });
      }
      localStorage.setItem("cart", JSON.stringify(cart));
      return cart;
    });
  };

  const incrementCartItem = (code) => {
    setCart((current) => {
      const cart = [...current];

      const index = cart.findIndex((el) => el.code === code);

      if (index !== -1) {
        setCards((current) => {
          const cards = [...current];
          const i = cards.findIndex((el) => el.code === code);
          cards[i].qty += 1;
          localStorage.setItem("cards", JSON.stringify(cards));
          return cards;
        });
        cart[index].qty += 1;
      }

      localStorage.setItem("cart", JSON.stringify(cart));
      return cart;
    });
  };

  const decrementCartItem = (code) => {
    setCart((current) => {
      const cart = [...current];

      const index = cart.findIndex((el) => el.code === code);

      if (index !== -1 && cart[index].qty > 1) {
        setCards((current) => {
          const cards = [...current];
          const i = cards.findIndex((el) => el.code === code);
          cards[i].qty -= 1;
          localStorage.setItem("cards", JSON.stringify(cards));
          return cards;
        });
        cart[index].qty -= 1;
      }

      localStorage.setItem("cart", JSON.stringify(cart));
      return cart;
    });
  };

  const deleteCartItem = (code) => {
    setCart((current) => {
      const cart = [...current];

      const index = cart.findIndex((el) => el.code === code);

      if (index !== -1) {
        setCards((current) => {
          const cards = [...current];
          const i = cards.findIndex((el) => el.code === code);
          cards[i].inCart = false;
          localStorage.setItem("cards", JSON.stringify(cards));
          return cards;
        });
        cart.splice(index, 1);
      }
      localStorage.setItem("cart", JSON.stringify(cart));
      return cart;
    });
  };

  const totalItemsInCart = () => {
    const cart = JSON.parse(localStorage.getItem("cart"));
    if (cart) {
      let qty = cart.map((item) => item.qty);
      let result = qty.reduce((sum, current) => sum + current, 0);
      return result;
    }
    return 0;
  };

  totalItemsInCart();

  const getSum = () => {
    const cart = JSON.parse(localStorage.getItem("cart"));
    if (cart) {
      let itemSum = cart.map((item) => item.qty * item.price);
      let result = itemSum.reduce((sum, current) => sum + current, 0);
      return result;
    }
  };

  getSum();

  return (
    <div className={styles.App}>
      <Header
        favorites={favorites}
        cart={cart}
        totalItemsInCart={totalItemsInCart}
      />

      <AppRoutes
        isCardsLoading={isCardsLoading}
        cards={cards}
        addToFavorites={addToFavorites}
        toggleModal={toggleModal}
        setModalProps={setModalProps}
        cart={cart}
        modalProps={modalProps}
        addToCart={addToCart}
        favorites={favorites}
        incrementCartItem={incrementCartItem}
        decrementCartItem={decrementCartItem}
        deleteCartItem={deleteCartItem}
        isOpenModal={isOpenModal}
        getSum={getSum}
      />
    </div>
  );
};

export default App;
