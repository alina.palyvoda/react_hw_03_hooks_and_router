import styles from "./Modal.module.scss";
import PropTypes from "prop-types";

const Modal = (props) => {
  const { children, toggleModal, actions } = props;

  return (
    <div className={styles.modalContainer}>
      <div
        onClick={(e) => e.currentTarget === e.target && toggleModal(false)}
        className={styles.background}
      >
        <div className={styles.content}>
          <p>{children}</p>
          <div>{actions}</div>
        </div>
      </div>
    </div>
  );
};

Modal.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]).isRequired,
  toggleModal: PropTypes.func,
  actions: PropTypes.object.isRequired,
};

Modal.defaultProps = {
  toggleModal: () => {},
};

export default Modal;
