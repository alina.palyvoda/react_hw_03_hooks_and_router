import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import styles from "./Header.module.scss";
import Navigation from "../Navigation/Navigation";

const Header = (props) => {
  const { favorites, totalItemsInCart } = props;

  return (
    <header className={styles.container}>

      <NavLink to="/" className={styles.logo}>
        <h1 className={styles.logoName}>SOFTLY</h1>
        <span>магазин м'яких іграшок</span>
      </NavLink>

      <Navigation
        favorites={favorites}
        totalItemsInCart={totalItemsInCart}
        className={styles.navMenu}
      />
      
    </header>
  );
};

Header.propTypes = {
  favorites: PropTypes.array,
  totalItemsInCart: PropTypes.func,
};

Header.defaultProps = {
  favorites: [],
  totalItemsInCart: () => {},
};

export default Header;
