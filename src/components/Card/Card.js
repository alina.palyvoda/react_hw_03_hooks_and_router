import PropTypes from "prop-types";
import styles from "./Card.module.scss";
import Button from "../Button/Button";

const Card = (props) => {
  const {
    title,
    imgSrc,
    price,
    color,
    code,
    isFavorite,
    inCart,
    addToFavorites,
    toggleModal,
    setModalProps,
  } = props;

  return (
    <div className={styles.container}>
      <img
        onClick={() => {
          addToFavorites({ title, imgSrc, price, code, isFavorite, inCart });
        }}
        className={styles.likeButton}
        alt={"like"}
        src={isFavorite ? "./images/like.png" : "./images/like-empty.png"}
      ></img>
      <a href="#" className={styles.wrapper}>
        <img className={styles.avatar} alt={title} src={imgSrc}></img>
        <h4 className={styles.title}>{title}</h4>
      </a>
      <span className={styles.price}>{price} грн</span>

      <Button
        children={inCart ? "Товар у кошику" : "У кошик"}
        onClick={() => {
          toggleModal(true);
          setModalProps({ title, price, code, imgSrc, inCart, isFavorite });
        }}
        className={inCart ? styles.pressedButton : styles.buyButton}
      ></Button>

      <div className={styles.description}>
        <span>Артикул: {code}</span>
        <span>Колір: {color}</span>
      </div>
    </div>
  );
};

Card.propTypes = {
  title: PropTypes.string.isRequired,
  imgSrc: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  code: PropTypes.number.isRequired,
  color: PropTypes.string,
  isFavorite: PropTypes.bool,
  inCart: PropTypes.bool,
  toggleModal: PropTypes.func,
  addToFavorites: PropTypes.func,
  setModalProps: PropTypes.func,
};

Card.defaultProps = {
  toggleModal: () => {},
  setModalProps: () => {},
  addToFavorites: () => {},
  isFavorite: false,
  inCart: false,
  color: "різнокольоровий",
};

export default Card;
