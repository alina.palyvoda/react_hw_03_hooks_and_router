import { NavLink } from "react-router-dom";
import styles from "./Navigation.module.scss";
import PropTypes from "prop-types";

const Navigation = (props) => {
  const { favorites, totalItemsInCart } = props;

  return (
    <nav>
      <ul className={styles.navMenu}>
        <li>
          <NavLink to="/">Магазин</NavLink>
        </li>
        <li>
          <NavLink to="/favorites">
            <span>Обране</span>
            <span className={styles.qty}>({favorites.length})</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/cart">
            <span>Кошик</span>
            <span className={styles.qty}>({ totalItemsInCart() })</span>
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};

Navigation.propTypes = {
  favorites: PropTypes.array,
  totalItemsInCart: PropTypes.func,
};

Navigation.defaultProps = {
  favorites: [],
  totalItemsInCart: () => {}
};

export default Navigation;
