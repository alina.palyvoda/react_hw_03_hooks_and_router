import PropTypes from "prop-types";
import styles from "./CartItem.module.scss";
import Button from "../Button/Button";

const CartItem = (props) => {
  const {
    title,
    imgSrc,
    price,
    code,
    isFavorite,
    toggleModal,
    setModalProps,
    inCart,
    qty,
    incrementCartItem,
    decrementCartItem,
  } = props;

  

  return (
    <div className={styles.container}>
      <a href="#" className={styles.avatar}>
        <img alt={title} src={imgSrc}></img>
      </a>
      <div className={styles.wrapper}>
        <a href="#" className={styles.title}>
          <h4>{title} </h4>
          <span className={styles.price}>{qty*price} грн</span>
        </a>
        <div>
        <Button onClick={() => decrementCartItem(code)}>-</Button>
        <span className={styles.itemQty}>{qty}</span>
        <Button onClick={() => incrementCartItem(code)}>+</Button>
        <span className={styles.closeBtn} onClick={() => {
          toggleModal(true); 
          setModalProps({ title, price, code, imgSrc, inCart, isFavorite })
        }
        }>x</span>
        
        </div>
      </div>
    </div>
  );
};

CartItem.propTypes = {
  title: PropTypes.string.isRequired,
  imgSrc: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  code: PropTypes.number.isRequired,
  qty: PropTypes.number.isRequired,
  isFavorite: PropTypes.bool,
  toggleModal: PropTypes.func,
  addToFavorites: PropTypes.func,
  setModalProps: PropTypes.func,
  incrementCartItem: PropTypes.func,
  decrementCartItem: PropTypes.func,
};

CartItem.defaultProps = {
  toggleModal: () => {},
  setModalProps: () => {},
  addToFavorites: () => {},
  isFavorite: false,
  incrementCartItem: () => {},
  decrementCartItem: () => {},
};

export default CartItem;
