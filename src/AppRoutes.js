import { Routes, Route } from "react-router-dom";
import HomePage from "./pages/HomePage/HomePage";
import FavoritePage from "./pages/FavoritePage/FavoritePage";
import CartPage from "./pages/CartPage/CartPage";

const AppRoutes = (props) => {
  const {
    isCardsLoading,
    cards,
    addToFavorites,
    toggleModal,
    setModalProps,
    cart,
    modalProps,
    addToCart,
    favorites,
    isOpenModal,
    incrementCartItem,
    decrementCartItem,
    deleteCartItem,
    getSum,
  } = props;

  return (
    <Routes>
      <Route
        path="/"
        element={
          <HomePage
            isCardsLoading={isCardsLoading}
            cards={cards}
            addToFavorites={addToFavorites}
            toggleModal={toggleModal}
            setModalProps={setModalProps}
            modalProps={modalProps}
            addToCart={addToCart}
            isOpenModal={isOpenModal}
          />
        }
      />

      <Route
        path="/favorites"
        element={
          <FavoritePage
            favorites={favorites}
            cards={cards}
            addToFavorites={addToFavorites}
            toggleModal={toggleModal}
            setModalProps={setModalProps}
            modalProps={modalProps}
            addToCart={addToCart}
            isOpenModal={isOpenModal}
          />
        }
      />
      <Route
        path="/cart"
        element={
          <CartPage
            cards={cards}
            cart={cart}
            incrementCartItem={incrementCartItem}
            decrementCartItem={decrementCartItem}
            deleteCartItem={deleteCartItem}
            modalProps={modalProps}
            setModalProps={setModalProps}
            isOpenModal={isOpenModal}
            toggleModal={toggleModal}
            getSum={getSum}
          />
        }
      />
    </Routes>
  );
};

export default AppRoutes;
