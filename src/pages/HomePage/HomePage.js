import Card from "../../components/Card/Card";
import styles from "./HomePage.module.scss";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/Button";
import PropTypes from "prop-types";

const HomePage = (props) => {
  const {
    isCardsLoading,
    cards,
    addToFavorites,
    toggleModal,
    setModalProps,
    modalProps,
    addToCart,
    isOpenModal,
  } = props;
  const { title, price, code, imgSrc, inCart, isFavorite } = modalProps;

  return (
    <>
      {isCardsLoading ? (
        <h2>Loading...</h2>
      ) : (
        <main className={styles.container}>
          {cards.map(
            ({ title, price, src, code, color, isFavorite, inCart }) => (
              <Card
                key={code}
                title={title}
                imgSrc={src}
                price={price}
                color={color}
                code={code}
                addToFavorites={addToFavorites}
                isFavorite={isFavorite}
                inCart={inCart}
                toggleModal={toggleModal}
                setModalProps={setModalProps}
              />
            )
          )}

          {isOpenModal && (
            <Modal
              toggleModal={toggleModal}            
              actions={
                <>
                  <Button
                    onClick={() => {
                      addToCart({ title, price, code, imgSrc, inCart, isFavorite });
                      toggleModal(false);
                    }}
                    type="submit">Yes</Button>
                  <Button
                    onClick={() => {
                      toggleModal(false);
                    }}
                    type="submit">No</Button>
                </>
              }
            >
              Ви дійстно бажаєте додати у кошик товар "{title}" за {price}грн?
            </Modal>
          )}
        </main>
      )}
    </>
  );
};

HomePage.propTypes = {
  isOpenModal: PropTypes.bool,
  isCardsLoading: PropTypes.bool,
  cards: PropTypes.array,
  addToCart: PropTypes.func,
  modalProps: PropTypes.object,
  toggleModal: PropTypes.func,
};

HomePage.defaultProps = {
  isOpenModal: false,
  isCardsLoading: true,
  cards: [],
  modalProps: {},
  toggleModal: () => {},
  addToCart: () => {},
};

export default HomePage;
