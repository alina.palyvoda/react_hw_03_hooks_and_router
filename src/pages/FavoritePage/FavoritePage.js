import Card from "../../components/Card/Card";
import styles from "./FavoritePage.module.scss";
import Modal from "../../components/Modal/Modal";
import PropTypes from "prop-types";

const FavoritePage = (props) => {
  const {
    cards,
    favorites,
    addToFavorites,
    toggleModal,
    setModalProps,
    addToCart,
    modalProps,
    isOpenModal,
  } = props;
  const { title, price, code, imgSrc, inCart, isFavorite } = modalProps;

  if (favorites.length === 0) {
    return <p>Обраних товарів немає...</p>;
  }

  return (
    <>
      <h3>Обрані товари</h3>
      <main className={styles.container}>
        {cards.map(
          ({ title, price, src, code, color, isFavorite, inCart }) =>
            isFavorite && (
              <Card
                key={code}
                title={title}
                imgSrc={src}
                price={price}
                color={color}
                code={code}
                addToFavorites={addToFavorites}
                isFavorite={isFavorite}
                inCart={inCart}
                toggleModal={toggleModal}
                setModalProps={setModalProps}
              />
            )
        )}

        {isOpenModal && (
          <Modal
            toggleModal={toggleModal}
            actions={
              <>
                <button
                  onClick={() => {
                    addToCart({
                      title,
                      price,
                      code,
                      imgSrc,
                      inCart,
                      isFavorite,
                    });
                    toggleModal(false);
                  }}
                  type="submit"
                >
                  Yes
                </button>
                <button
                  onClick={() => {
                    toggleModal(false);
                  }}
                  type="submit"
                >
                  No
                </button>
              </>
            }
          >
            Ви дійстно бажаєте додати у кошик товар "{title}" за {price}грн?
          </Modal>
        )}
      </main>
    </>
  );
};

FavoritePage.propTypes = {
  isOpenModal: PropTypes.bool,
  cards: PropTypes.array,
  addToCart: PropTypes.func,
  modalProps: PropTypes.object,
  toggleModal: PropTypes.func,
};

FavoritePage.defaultProps = {
  isOpenModal: false,
  cards: [],
  modalProps: {},
  toggleModal: () => {},
  addToCart: () => {},
};

export default FavoritePage;
