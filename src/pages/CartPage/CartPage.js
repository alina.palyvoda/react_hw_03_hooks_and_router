import Button from "../../components/Button/Button";
import CartItem from "../../components/CartItem/CartItem";
import styles from "./CartPage.module.scss";
import Modal from "../../components/Modal/Modal";
import PropTypes from "prop-types";

const CartPage = (props) => {
  const {
    cards,
    cart,
    toggleModal,
    setModalProps,
    incrementCartItem,
    decrementCartItem,
    deleteCartItem,
    modalProps,
    isOpenModal,
    getSum,
  } = props;

  const { title, code } = modalProps;

  if (cart.length === 0) {
    return <p>Кошик порожній</p>;
  }

  return (
    <>
      <h3>Товари у кошику</h3>
      <main className={styles.container}>
        {cards.map(
          ({ title, price, src, code, isFavorite, inCart, qty }) =>
            inCart && (
              <CartItem
                key={code}
                title={title}
                imgSrc={src}
                price={price}
                code={code}
                isFavorite={isFavorite}
                inCart={inCart}
                toggleModal={toggleModal}
                setModalProps={setModalProps}
                qty={qty}
                incrementCartItem={incrementCartItem}
                decrementCartItem={decrementCartItem}
              />
            )
        )}

        {isOpenModal && (
          <Modal
            toggleModal={toggleModal}
            actions={
              <>
                <Button
                  onClick={() => {
                    deleteCartItem(code)
                    toggleModal(false);
                  }}
                  type="submit"
                >
                  Yes
                </Button>
                <Button
                  onClick={() => {
                    toggleModal(false);
                  }}
                  type="submit"
                >
                  No
                </Button>
              </>
            }
          >
            Ви дійстно бажаєте видалити з кошика товар "{title}"?
          </Modal>
        )}
      </main>
      <p className={styles.total}>Разом:    {getSum()}грн</p>
      <Button className={styles.orderBtn}>Оформити замовлення</Button>
    </>
  );
};

CartPage.propTypes = {
  isOpenModal: PropTypes.bool,
  cards: PropTypes.array,
  cart: PropTypes.array,
  addToCart: PropTypes.func,
  modalProps: PropTypes.object,
  toggleModal: PropTypes.func,
  deleteCartItem: PropTypes.func,
  getSum: PropTypes.func,
};

CartPage.defaultProps = {
  isOpenModal: false,
  cart: [],
  cards: [],
  modalProps: {},
  toggleModal: () => {},
  addToCart: () => {},
  deleteCartItem: () => {},
  getSum: () => {},
};

export default CartPage;
